import $ from 'jQuery';

function renderDescription(description) {
  $('#description').html(description);
}

export default renderDescription;
