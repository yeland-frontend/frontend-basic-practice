import fetchData from './resourses/fetchData';
import createPerson from './utils/createPerson';
import renderBasicInfo from './views/renderBasicInfo';
import renderDescription from './views/renderDescription';
import renderEducations from './views/renderEducations';

fetchData(URL)
  .then(response => {
    const person = createPerson(response);
    renderBasicInfo(person);
    renderDescription(person.description);
    renderEducations(person.educations);
  })
  .catch(error => error);
