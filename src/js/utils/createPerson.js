import Person from './Person';

function createPerson(response) {
  const { name, age, description, educations } = response;
  return new Person(name, age, description, educations);
}

export default createPerson;
