function fetchData() {
  const URL = 'http://localhost:3000/person';
  return fetch(URL).then(response => response.json());
}

export default fetchData;
