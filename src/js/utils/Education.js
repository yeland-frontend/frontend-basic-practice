class Education {
  constructor(year, title, description) {
    this.year = year;
    this.title = title;
    this.description = description;
  }
}

export default Education;
