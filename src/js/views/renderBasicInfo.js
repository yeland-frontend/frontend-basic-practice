import $ from 'jQuery';

function renderBasicInfo(person) {
  $('#name').html(person.name);
  $('#age').html(person.age);
}

export default renderBasicInfo;
