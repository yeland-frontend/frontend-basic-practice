import $ from 'jQuery';

function renderEducations(educations) {
  let location = $('#education');
  educations.forEach(education =>
    location.append(`<li>
      <section class = "year">${education.year}</section>
      <section class = "school">
        <p><strong>${education.title}</strong></p>
        <p>${education.description}</p>
      </section>
    </li>`)
  );
}

export default renderEducations;
