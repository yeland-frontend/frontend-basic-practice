import Education from './Education';

class Person {
  constructor(name, age, description, educations) {
    this.name = name;
    this.age = age;
    this.description = description;
    this.educations = educations.map(education => {
      const { year, title, description } = education;
      return new Education(year, title, description);
    });
  }
}

export default Person;
